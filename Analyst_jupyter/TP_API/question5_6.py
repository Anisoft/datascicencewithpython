import numpy as np
import pandas as pd

def format_data(piece_of_data:dict) -> dict:
    """
    Cette fonction transforme un dictionnaire contennant éventuellement des dictionnaires en un dictionnaire unifié avec les dictionnaires y contenus
    Input :
        piece_of_data:dict => données correspondant à un bus
    Output :
        result:dict => le paramètre «piece_of_data» formaté en dictionnaire unifié
    """
    result:dict() = dict()
    for element in data.keys():
        if type(d2[element]) == dict:
            result.update(data[element])
        else:
            result[element] = data[element]
    return result

def conversion_of_data(data:list):
    """
    Cette fonction applique la fonction «format_data» à toutes les lignes de données de bus
    Input :
        data:dict => toutes les données de tous les bus
    Output :
        result:dict => le paramètre «data» dont tous les éléments sont formatés chacun en dictionnaire unifié
    """
    liste_data:list = []
    for i in range(len(data)):
        for j in range(len(data[i])):
            liste_data.append(format_data(data[i][j]))
    return liste_data
# CRÉATION DU DATAFRAME
df = pd.DataFrame(conversion_of_data(data))
df