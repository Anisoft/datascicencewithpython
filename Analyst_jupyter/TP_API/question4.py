# 4. Function poll_data(n_minutes) avec sleep
import requests as req
import json
import time
def poll_data(n_minutes: int) :
    donnees = []
    for i in range(n_minutes) :
        donnees.append(req.get("https://data.opendatasoft.com/api/records/1.0/search/?dataset=cooling-towers-and-evaporative-condensers%40bristol").json()['records'])
        time.sleep(60)
        print(i+1, " minute(s)")
    return donnees

print(poll_data(1))