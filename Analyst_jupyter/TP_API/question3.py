import requests as req
import json

# 3. Functions
def get_velib_data(nrows : int) :
    velos = []
    velo = req.get(f"https://data.opendatasoft.com/api/datasets/1.0/search/?q=&rows={nrows}")
    for data in velo.json()['datasets']:
        velos.append(data)
    return velos

print(get_velib_data(2))