import requests as req
import json

# 2. Request
dataset_id = 'data-es-activites@equipements-sgsocialgouv'
export_format = 'json'
# velo = req.get('https://data.opendatasoft.com/api/v2/catalog/datasets/' + dataset_id + '/records')
velo = req.get(f"https://data.opendatasoft.com/api/datasets/1.0/search/?q={dataset_id}/records/")
data = velo.text
json_data = json.loads(data)
print(json_data)