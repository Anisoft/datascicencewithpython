# Partie 0

# 1. une requête HTTP Get permet de récupérer des données par le biais d'une requetes
# sans rien passer en paramètre contrairement à une requête HTTP Post qui envoie une 
# requête avec paramètre(s) d'entrée(s) au serveur. Cette dernière requête peut avoir 
# ou non un retour. Mais quelque soit la nature de la requête HTTP, nous avons un statut
# de retour

# 2. Test de requête avec PostMan
#{
#    "message": "good job!",
#    "data": [
#        1,
#        42,
#        1,
#        42
#    ]
#}

# 3. 
import requests as req
import json
res = req.get('https://nowledgeable.com/http-exercice')
data = res.text
json_data = json.loads(data)
# print(json_data)

# PARTIE 1
# 1. Exploration de données

